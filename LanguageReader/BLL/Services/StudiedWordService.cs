﻿using System.Collections.Generic;
using System.Linq;
using LangRead.BLL.Interfaces;
using LangRead.DAL.Interfaces;
using LangRead.DAL.Entities;

namespace LangRead.BLL.Services
{
    public class StudiedWordService : IStudiedWordService
    {
        private readonly IUnitOfWork _database;

        public StudiedWordService(IUnitOfWork uow)
        {
            _database = uow;
        }

        public StudiedWord SaveNewStudiedWordToDb(string word, User user)
        {
            var studiedWord = new StudiedWord();
            var searchedWord = _database.StudiedWords.GetOneByName(word);
            if (searchedWord != null)
            {
                studiedWord = searchedWord;
            }
            else
            {
                studiedWord.Word = word;
                _database.StudiedWords.Create(studiedWord);
            }
            //studiedWord.Language = _readerContext.Languages.FirstOrDefault(l => l.LanguageName == language);
            studiedWord.Users.Add(user);
            _database.Users.Update(user);
            _database.Save();
            return studiedWord;
        }

        public List<StudiedWord> ReturnAllStudiedWordFromDb()
        {
            return _database.StudiedWords.GetAll().ToList();
        }

        public StudiedWord ReturnStudiedWordFromDbById(int id)
        {
            return _database.StudiedWords.GetOneById(id);
        }

        public StudiedWord ReturnStudiedWordFromDbByOneParameter(string singleParameter)
        {
            return _database.StudiedWords.GetOneByName(singleParameter);
        }

        public bool IsThisWordExistInDb(string word)
        {
            var wordWithUsers = _database.StudiedWords.CheckExistingByName(word);
            if (wordWithUsers != null)
            {
                return true;
            }
            return false;
        }

        public bool DoesUserHaveThisWord(string word, User user)
        {
            var wordWithUsers = _database.StudiedWords.CheckExistingByName(word);
            if (wordWithUsers != null && wordWithUsers.Users.Any(u => u.Login == user.Login))
            {
                return true;
            }
            return false;
        }

        public StudiedWord ChangeStudiedWordPropertiesInDb(StudiedWord studiedWord)
        {
            _database.StudiedWords.Update(studiedWord);
            _database.Save();
            return studiedWord;
        }

        public StudiedWord RemoveUserFromStudiedWordInDb(string word, User user)
        {
            var wordWithUsers = _database.StudiedWords.GetOneByName(word);
            var isUserRemoved = wordWithUsers.Users.RemoveAll(u => u.Id == user.Id);

            _database.StudiedWords.Update(wordWithUsers);
            _database.Save();
            return wordWithUsers;
        }

        public void Dispose()
        {
            _database.Dispose();
        }
    }
}
