﻿using LangRead.BLL.Interfaces;
using LangRead.DAL.Entities;
using LangRead.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace LangRead.BLL.Services
{
    public class KnownWordService : IKnownWordService
    {
        private readonly IUnitOfWork _database;

        public KnownWordService(IUnitOfWork uow)
        {
            _database = uow;
        }

        public KnownWord SaveNewKnownWordToDb(string word, User user)
        {
            var knownWord = new KnownWord();
            var searchedWord = _database.KnownWords.GetOneByName(word);
            if (searchedWord != null)
            {
                knownWord = searchedWord;
            }
            else
            {
                knownWord.Word = word;
                _database.KnownWords.Create(knownWord);
            }
            knownWord.Users.Add(user);
            _database.Users.Update(user);
            _database.Save();
            return knownWord;
        }

        public List<KnownWord> ReturnAllKnownWordFromDb()
        {
            return _database.KnownWords.GetAll().ToList();
        }

        public KnownWord ReturnKnownWordFromDbById(int id)
        {
            return _database.KnownWords.GetOneById(id);
        }

        public KnownWord ReturnKnownWordFromDbByOneParameter(string singleParameter)
        {
            return _database.KnownWords.GetOneByName(singleParameter);
        }

        public bool IsThisWordExistInDb(string word)
        {
            var wordWithUsers = _database.KnownWords.CheckExistingByName(word);
            if (wordWithUsers != null)
            {
                return true;
            }
            return false;
        }

        public bool DoesUserHaveThisWord(string word, User user)
        {
            var wordWithUsers = _database.StudiedWords.CheckExistingByName(word);
            if (wordWithUsers != null && wordWithUsers.Users.Any(u => u.Login == user.Login))
            {
                return true;
            }
            return false;
        }

        public KnownWord ChangeKnownWordPropertiesInDb(KnownWord knownWord)
        {
            _database.KnownWords.Update(knownWord);
            _database.Save();
            return knownWord;
        }

        public KnownWord RemoveUserFromKnownWordInDb(string word, User user)
        {
            var wordWithUsers = _database.KnownWords.GetOneByName(word);
            var isUserRemoved = wordWithUsers.Users.RemoveAll(u => u.Id == user.Id);

            _database.KnownWords.Update(wordWithUsers);
            _database.Save();
            return wordWithUsers;
        }

        public void Dispose()
        {
            _database.Dispose();
        }
    }
}
