﻿using LangRead.BLL.Interfaces;
using LangRead.DAL.Entities;
using LangRead.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace LangRead.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _database;

        public UserService(IUnitOfWork uow)
        {
            _database = uow;
        }
      
        public User SaveNewUserToDb(string login)
        {
            var user = _database.Users.Create(new User { Login = login });
            _database.Save();
            return user;
        }

        public List<User> ReturnAllUsersFromDb()
        {
            return _database.Users.GetAll().ToList();
        }

        public User ReturnUserFromDbByOneParameter(string login)
        {
            return _database.Users.GetOneByName(login);
        }

        public User ReturnUserFromDbById(int id)
        {
            return _database.Users.GetOneById(id);
        }

        public bool IsUserExistInDb(string login)
        {
            var currentUser = _database.Users.CheckExistingByName(login);
            if (currentUser != null)
            {
                return true;
            }
            return false;
        }

        public bool IsUserStudingThisWord(string login, string word)
        {
            var currentWord = _database.StudiedWords.CheckExistingByName(word);
            if (currentWord != null)
            {
                if (currentWord.Users.Any(u => u.Login == login))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsUserKnownThisWord(string login, string word)
        {
            var currentWord = _database.KnownWords.CheckExistingByName(word);
            if (currentWord != null)
            {
                if (currentWord.Users.Any(u => u.Login == login))
                {
                    return true;
                }
            }
            return false;
        }

        public User ChangeUserPropertiesInDb(User user)
        {
            _database.Users.Update(user);
            _database.Save();
            return user;
        }

        public void RemoveUserFromDb(User user)
        {
            _database.Users.Delete(user.Id);
            _database.Save();
        }

        public void Dispose()
        {
            _database.Dispose();
        }
    }
}
