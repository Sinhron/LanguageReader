﻿using LangRead.DAL.Entities;
using System.Collections.Generic;

namespace LangRead.BLL.Interfaces
{
    public interface IStudiedWordService
    {
        StudiedWord SaveNewStudiedWordToDb(string word, User user);
        List<StudiedWord> ReturnAllStudiedWordFromDb();
        StudiedWord ReturnStudiedWordFromDbById(int id);
        StudiedWord ReturnStudiedWordFromDbByOneParameter(string singleParameter);
        bool IsThisWordExistInDb(string word);
        bool DoesUserHaveThisWord(string word, User user);
        StudiedWord ChangeStudiedWordPropertiesInDb(StudiedWord studiedWord);
        StudiedWord RemoveUserFromStudiedWordInDb(string word, User user);
        void Dispose();
    }
}
