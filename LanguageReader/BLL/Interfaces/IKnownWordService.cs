﻿using LangRead.DAL.Entities;
using System.Collections.Generic;

namespace LangRead.BLL.Interfaces
{
    public interface IKnownWordService
    {
        KnownWord SaveNewKnownWordToDb(string word, User user);
        List<KnownWord> ReturnAllKnownWordFromDb();
        KnownWord ReturnKnownWordFromDbById(int id);
        KnownWord ReturnKnownWordFromDbByOneParameter(string singleParameter);
        bool IsThisWordExistInDb(string word);
        bool DoesUserHaveThisWord(string word, User user);
        KnownWord ChangeKnownWordPropertiesInDb(KnownWord knownWord);
        KnownWord RemoveUserFromKnownWordInDb(string word, User user);
        void Dispose();
    }
}
