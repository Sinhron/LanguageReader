﻿using LangRead.DAL.Entities;
using System.Collections.Generic;

namespace LangRead.BLL.Interfaces
{
    public interface IUserService
    {
        User SaveNewUserToDb(string login);
        List<User> ReturnAllUsersFromDb();
        User ReturnUserFromDbById(int id);
        User ReturnUserFromDbByOneParameter(string singleParameter);
        bool IsUserExistInDb(string name);
        bool IsUserStudingThisWord(string login, string word);
        bool IsUserKnownThisWord(string login, string word);
        User ChangeUserPropertiesInDb(User user);
        void RemoveUserFromDb(User user);
        void Dispose();
    }
}
