﻿using System.ComponentModel.DataAnnotations;

namespace LangRead.DAL.Entities
{
    public class UserData
    {
        [Key]
        public int Id { get; set; }
        public string Login { get; set; }
        public string Language { get; set; }
    }
}
