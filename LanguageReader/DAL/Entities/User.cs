﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LangRead.DAL.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Login { get; set; }
        public List<StudiedWord> StudiedWords { get; set; } = new List<StudiedWord>();
        public List<KnownWord> KnownWords { get; set; } = new List<KnownWord>();

    }
}
