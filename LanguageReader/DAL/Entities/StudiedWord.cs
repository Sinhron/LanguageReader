﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LangRead.DAL.Entities
{
    //Слова, которые учит юзер
    [Index(nameof(Word), IsUnique = true)]

    public class StudiedWord
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Word { get; set; }
        public List<User> Users { get; set; } = new List<User>();
        //public Language Language { get; set; }
    }
}
