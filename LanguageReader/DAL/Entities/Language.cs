﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LangRead.DAL.Entities
{
    public class Language
    {
        public int Id { get; set; }
        [Required]
        public string LanguageName { get; set; }
        public List<StudiedWord> StudiedWords { get; set; } = new List<StudiedWord>();
        public List<KnownWord> KnownWords { get; set; } = new List<KnownWord>();
    }
}
