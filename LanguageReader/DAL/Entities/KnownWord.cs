﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LangRead.DAL.Entities
{
    //Слова, которые уже знает юзер
    [Index(nameof(Word), IsUnique = true)]
    public class KnownWord
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Word { get; set; }
        public List<User> Users { get; set; } = new List<User>();
        //public Language Language { get; set; }
    }
}
