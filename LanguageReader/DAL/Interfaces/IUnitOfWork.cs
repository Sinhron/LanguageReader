﻿using LangRead.DAL.Entities;
using System;

namespace LangRead.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<StudiedWord> StudiedWords { get; }
        IRepository<KnownWord> KnownWords { get; }
        IRepository<User> Users { get; }
        void Save();
    }
}
