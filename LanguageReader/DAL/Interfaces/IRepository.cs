﻿using System;
using System.Collections.Generic;

namespace LangRead.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T Create(T item);
        IEnumerable<T> GetAll();
        T GetOneByName(string nameOrWord);
        T GetOneById(int id);
        T CheckExistingByName(string nameOrWord);
        IEnumerable<T> FindAll(Func<T, Boolean> predicate);
        void Update(T item);
        void Delete(int id);
    }
}