﻿using LangRead.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace LangRead.Context
{
    public class ReaderContext : DbContext
    {
        public ReaderContext(DbContextOptions<ReaderContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<KnownWord> KnownWords { get; set; }
        public DbSet<StudiedWord> StudiedWords { get; set; }
        //public DbSet<Language> Languages { get; set; }

        public void DetachAllEntities()
        {
            var changedEntriesCopy = this.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in changedEntriesCopy)
                entry.State = EntityState.Detached;
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=LanguageReaderDB;Integrated Security=true;");
        //}

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<User>()
        //        .HasMany(x => x.StudiedWords)
        //        .WithMany(x => x.Users)
        //        .UsingEntity(x => x.ToTable("UserStudied"));
        //}
    }
}
