﻿using LangRead.Context;
using LangRead.DAL.Entities;
using System;
using LangRead.DAL.Interfaces;

namespace LangRead.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private ReaderContext db;
        private StudiedWordRepository studiedWordRepository;
        private KnownWordRepository knownWordRepository;
        private UserRepository userRepository;

        public EFUnitOfWork(ReaderContext readerContext)
        {
            db = readerContext;
        }
        public IRepository<StudiedWord> StudiedWords
        {
            get
            {
                if (studiedWordRepository == null)
                {
                    studiedWordRepository = new StudiedWordRepository(db);
                }
                return studiedWordRepository;
            }
        }

        public IRepository<KnownWord> KnownWords
        {
            get
            {
                if (knownWordRepository == null)
                {
                    knownWordRepository = new KnownWordRepository(db);
                }
                return knownWordRepository;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(db);
                }
                return userRepository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
