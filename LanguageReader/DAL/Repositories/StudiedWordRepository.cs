﻿using LangRead.DAL.Interfaces;
using LangRead.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using LangRead.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace LangRead.Repositories
{
    public class StudiedWordRepository : IRepository<StudiedWord>
    {
        private readonly ReaderContext _dbContext;
        public StudiedWordRepository(ReaderContext context)
        {
            _dbContext = context;
        }

        public StudiedWord Create(StudiedWord studiedWord)
        {
            _dbContext.StudiedWords.Add(studiedWord);
            _dbContext.Entry(studiedWord).State = EntityState.Added;
            return studiedWord;
        }

        public IEnumerable<StudiedWord> GetAll()
        {
            return _dbContext.StudiedWords;
        }

        public StudiedWord GetOneById(int id)
        {
            return _dbContext.StudiedWords.Find(id);
        }

        public StudiedWord GetOneByName(string word)
        {
            return _dbContext.StudiedWords.Include(c => c.Users).FirstOrDefault(w => w.Word == word);
        }

        public StudiedWord CheckExistingByName(string word)
        {
            return _dbContext.StudiedWords.AsNoTracking().Include(c => c.Users).FirstOrDefault(w => w.Word == word);
        }

        public IEnumerable<StudiedWord> FindAll(Func<StudiedWord, bool> predicate)
        {
            return _dbContext.StudiedWords.Include(c => c.Users).Where(predicate).ToList();
        }

        public void Update(StudiedWord studiedWord)
        {
            _dbContext.Entry(studiedWord).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var studiedWord = _dbContext.StudiedWords.Find(id);
            if (studiedWord is null)
            {
                throw new ArgumentException("Studied word has been not found");
            }
            _dbContext.StudiedWords.Remove(studiedWord);
            _dbContext.SaveChanges();
        }
    }
}
