﻿using LangRead.DAL.Interfaces;
using LangRead.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using LangRead.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace LangRead.Repositories
{
    public class KnownWordRepository : IRepository<KnownWord>
    {
        private readonly ReaderContext _dbContext;
        public KnownWordRepository(ReaderContext context)
        {
            _dbContext = context;
        }

        public KnownWord Create(KnownWord knownWord)
        {
            _dbContext.KnownWords.Add(knownWord);
            _dbContext.Entry(knownWord).State = EntityState.Added;
            return knownWord;
        }

        public IEnumerable<KnownWord> GetAll()
        {
            return _dbContext.KnownWords;
        }

        public KnownWord GetOneById(int id)
        {
            return _dbContext.KnownWords.Find(id);
        }

        public KnownWord GetOneByName(string word)
        {
            return _dbContext.KnownWords.Include(c => c.Users).FirstOrDefault(w => w.Word == word);
        }

        public KnownWord CheckExistingByName(string word)
        {
            return _dbContext.KnownWords.AsNoTracking().Include(c => c.Users).FirstOrDefault(w => w.Word == word);
        }

        public IEnumerable<KnownWord> FindAll(Func<KnownWord, bool> predicate)
        {
            return _dbContext.KnownWords.Where(predicate).ToList();
        }

        public void Update(KnownWord knownWord)
        {
            _dbContext.Entry(knownWord).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var knownWord = _dbContext.KnownWords.Find(id);
            if (knownWord is null)
            {
                throw new ArgumentException("Known word word has been not found");
            }
            _dbContext.KnownWords.Remove(knownWord);
            _dbContext.SaveChanges();
        }
    }
}
