﻿using LangRead.DAL.Interfaces;
using LangRead.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using LangRead.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace LangRead.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ReaderContext _dbContext;
        public UserRepository(ReaderContext context)
        {
            _dbContext = context;
        }

        public User Create(User user)
        {
            _dbContext.Users.Add(user);
            return user;
        }

        public IEnumerable<User> GetAll()
        {
            return _dbContext.Users;
        }

        public User GetOneById(int id)
        {
            return _dbContext.Users.Find(id);
        }

        public User GetOneByName(string name)
        {
            return _dbContext.Users.Include(u => u.StudiedWords).Include(u => u.KnownWords).FirstOrDefault(u => u.Login == name);
        }

        public User CheckExistingByName(string name)
        {
            return _dbContext.Users.AsNoTracking().Include(u => u.StudiedWords).Include(u => u.KnownWords).FirstOrDefault(u => u.Login == name);
        }

        public IEnumerable<User> FindAll(Func<User, bool> predicate)
        {
            return _dbContext.Users.Where(predicate).ToList();
        }

        public void Update(User user)
        {
            _dbContext.Entry(user).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var user = _dbContext.Users.Find(id);
            if (user is null)
            {
                throw new ArgumentException("User has been not found");
            }
            _dbContext.Users.Remove(user);
            _dbContext.SaveChanges();
        }
    }
}
