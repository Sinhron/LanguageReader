﻿using LangRead.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LangRead.BusinessLogic
{
    public class TextPreparer
    {

        public string GetWordSimbols(string someWord, out Dictionary<int, char> simbols)
        {
            var word = someWord.ToCharArray();
            var result = string.Empty;
            simbols = new Dictionary<int, char>();
            foreach (var letter in word)
            {
                if (Char.IsLetter(letter))
                {
                    result += letter.ToString();
                }
                else
                {
                    simbols.Add(Array.IndexOf(word, letter), letter);
                }
            }
            return result;
        }

        //public static void GetMulticoloredText(TextPointer startPosition, string text, string userLogin, Color mainWindowColor)
        //{
        //    var wordArray = text.Split(' ', ',', '.', '!', '?', '\"', '\'', ';', ':', '\'', '\\', '/', '-', '(', ')', '\t', '\r', '\n').ToList();
        //    var numberOfEmptyStrings = wordArray.RemoveAll(string.IsNullOrEmpty);

        //    for(int i = 0; i < wordArray.Count;)
        //    {
        //        if (_studiedRepository.IsThisWordAlreadyExistInDb(wordArray[i], userLogin))
        //        {
        //            HighlightWords(startPosition, " " + wordArray[i] + " ", text, Colors.BlueViolet);
        //        }
        //        else if (_knownRepository.IsThisWordAlreadyExistInDb(wordArray[i], userLogin))
        //        {
        //            HighlightWords(startPosition, " " + wordArray[i] + " ", text, mainWindowColor);
        //        }
        //        else
        //        {
        //            HighlightWords(startPosition, " " + wordArray[i] + " ", text, Colors.Brown);
        //        }
        //        var x = wordArray[i];
        //        var xLow = x.ToLower();
        //        var xUp = x.Substring(0, 1).ToUpper() + (x.Length > 1 ? wordArray[i].Substring(1) : "");
        //        var repeatableWords = wordArray.RemoveAll(a => a.Equals(xUp) || a.Equals(xLow));
        //    }
        //}

        //public static void HighlightWords(TextPointer startPosition, string searchWord, string text, Color color)
        //{
        //    int instancesOfSearchKey = Regex.Matches(text.ToLower(), searchWord.ToLower()).Count;

        //    for (int i = 0; i < instancesOfSearchKey; i++)
        //    {
        //        int lastInstanceIndex = HighlightNextInstance(startPosition, searchWord, color);
        //        if (lastInstanceIndex == -1)
        //        {
        //            break;
        //        }
        //        startPosition = startPosition.GetPositionAtOffset(lastInstanceIndex);
        //    }
        //}

        //private static int HighlightNextInstance(TextPointer startPosition, string searchWord, Color color)
        //{
        //    int indexOfLastInstance = -1;

        //    while (true)
        //    {
        //        TextPointer next = startPosition.GetNextContextPosition(LogicalDirection.Forward);
        //        if (next == null)
        //        {
        //            break;
        //        }
        //        TextRange newText = new TextRange(startPosition, next);

        //        int index = newText.Text.ToLower().IndexOf(searchWord.ToLower());
        //        if (index != -1)
        //        {
        //            indexOfLastInstance = index;
        //        }

        //        if (index > 0)
        //        {
        //            TextPointer start = startPosition.GetPositionAtOffset(index + 1);
        //            TextPointer end = startPosition.GetPositionAtOffset(index + searchWord.Length - 1);
        //            TextRange textRange = new TextRange(start, end);
        //            textRange.ApplyPropertyValue(TextElement.BackgroundProperty, new SolidColorBrush(color));
        //        }
        //        startPosition = next;
        //    }

        //    return indexOfLastInstance;
        //}
    }
}
