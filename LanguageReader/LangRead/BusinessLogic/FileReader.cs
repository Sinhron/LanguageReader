﻿using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LangRead.BusinessLogic
{
    public class FileReader
    {
        public static string Text;
        public static async Task<string> ReadTxtFile(string path)
        {
            try
            {
                using (var sr = new StreamReader(path))
                {
                    var text = await sr.ReadToEndAsync();
                    return text;
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        //public static async Task<string> Load_Data()
        //{
        //    var dlg = new OpenFileDialog();
        //    dlg.DefaultExt = ".txt"; // Расширение файла по умолчанию
        //    dlg.Filter = "Текстовые файлы |*.txt"; // Фильтрация файлов по расширению

        //    // Результаты процесса открытия окна
        //    if (dlg.ShowDialog() == true)
        //    {
        //        return await ReadTxtFile(dlg.FileName);
        //    }
        //    return null;
        //}

        //public static void LoadTheBook()
        //{
        //    var dlg = new OpenFileDialog();
        //    dlg.DefaultExt = ".txt"; // Расширение файла по умолчанию
        //    dlg.Filter = "Текстовые файлы |*.txt"; // Фильтрация файлов по расширению
        //    // Результаты процесса открытия окна
        //    if (dlg.ShowDialog() == true)
        //    {
        //        Text = File.ReadAllText(dlg.FileName);
        //    }
        //}
    }
}