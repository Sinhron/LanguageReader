using LangRead.BLL.Interfaces;
using LangRead.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace LangRead.Pages
{
    public class LoginModel : PageModel
    {
        [BindProperty]
        public string Login { get; set; }
        [BindProperty]
        public string Language { get; set; }
        public string Version { get; set; } = "1.00";

        private User user { get; set; }
        public string Message { get; set; }
        private readonly IUserService _userService;

        public LoginModel(IUserService userService)
        {
            _userService = userService;
        }

        public void OnGet()
        {
            Message = "������� ���:";
        }

        public IActionResult OnPost()
        {
            if (_userService.IsUserExistInDb(Login))
            {
                user = _userService.ReturnUserFromDbByOneParameter(Login);
                return RedirectToPage("MainPage", new UserData { Id = user.Id, Login = user.Login, Language = Language });
            }            
            Message = "�� ��� �� ���������������� ������ ���";
            return null;
        }
    }
}
