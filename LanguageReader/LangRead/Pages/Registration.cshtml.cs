using LangRead.BLL.Interfaces;
using LangRead.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace LangRead.Pages
{
    public class RegistrationModel : PageModel
    {
        public string Message { get; set; }
        [BindProperty]
        public User User { get; set; }
        public string Version { get; set; } = "1.00";


        private readonly IUserService _userService;

        public RegistrationModel(IUserService userService)
        {
            _userService = userService;
        }

        public void OnGet()
        {
            Message = "������� ���:";
        }

        public IActionResult OnPost(string login, string confirmLogin)
        {
            if (login != confirmLogin)
            {
                Message = "��������� ����� �� ���������, ���������� ��� ���";
                return null;
            }
            else if (_userService.IsUserExistInDb(login))
            {
                Message = "������������ � ����� ������� ��� ����������";
                return null;
            }
            else
            {
                User = _userService.SaveNewUserToDb(login);
                return RedirectToPage("Login");
            }
        }
    }
}
