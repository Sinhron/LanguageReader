using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using LangRead.BLL.Interfaces;
using LangRead.DAL.Entities;

namespace LangRead.Pages
{
    public class MainPageModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public static User User { get; set; }
        [BindProperty]
        public IFormFile Upload { get; set; }
        [BindProperty]
        public string Word { get; set; }
        [BindProperty]
        public int PageNumber { get; set; }

        public int Count { get; set; }
        public string TranslatedWord { get; set; } = "��� ������ ��������?";
        public List<string> TextOnPage { get; set; } = new List<string>();
        public string MessageRelatedWithWord { get; set; } = "������ -> ��� ���� -> ����� �����";
        public int CurrentPage { get; set; } = 0;
        public int HowManyPages { get; set; }
        public string Version { get; set; } = "1.00";


        private IHostingEnvironment _environment;
        private static string Language { get; set; }
        private static string lastBookPath { get; set; }
        private static List<string> WordsList { get; set; }
        private static int numberOfPage { get; set; } = 1;
        public static List<string> WordsOnPage { get; set; } = new List<string>();
        public static List<int> VisitedPages { get; set; } = new List<int>();
        public static string FilePath { get; set; }
        public static string FullText { get; set; }
        public static int QuantityOfPages { get; set; }



        private readonly IStudiedWordService _studiedWordService;
        private readonly IKnownWordService _knownWordService;
        private readonly IUserService _userService;

        public MainPageModel(IHostingEnvironment environment, IStudiedWordService studiedWordService, IKnownWordService knownWordService,
             IUserService userService)
        {
            _environment = environment;
            _studiedWordService = studiedWordService;
            _knownWordService = knownWordService;
            _userService = userService;
        }

        public void OnGet(UserData userData)
        {
            User = _userService.ReturnUserFromDbByOneParameter(userData.Login);
            Language = userData.Language;
        }

        public async Task OnPostOpenBook()
        {
            await OpenBookFirstTime();
            OpenPage(numberOfPage);
        }

        private async Task OpenBookFirstTime()
        {
            numberOfPage = 1;
            CurrentPage = numberOfPage;
            if (Upload != null)
            {
                lastBookPath = Upload.FileName;
                FilePath = Path.Combine(_environment.ContentRootPath, "Books", lastBookPath);
                using (var fileStream = new FileStream(FilePath, FileMode.OpenOrCreate))
                {
                    await Upload.CopyToAsync(fileStream);
                }
            }

            FilePath = Path.Combine(_environment.ContentRootPath, "Books", lastBookPath);
            using (var sr = new StreamReader(FilePath))
            {
                FullText = await sr.ReadToEndAsync();
            }
            QuantityOfPages = PrepareListAndCountPages(FullText);
        }

        private int PrepareListAndCountPages(string text)
        {
            WordsList = text.Split(' ').ToList();
            var numberOfEmptyStrings = WordsList.RemoveAll(string.IsNullOrEmpty);
            var quantityOfPages = WordsList.Count / 150;
            return quantityOfPages;
        }

        private void OpenPage(int numberOfPage)
        {
            HowManyPages = QuantityOfPages;
            TextOnPage = PreparePage(numberOfPage);
            WordsOnPage = TextOnPage;
            CurrentPage = numberOfPage;
        }

        private List<string> PreparePage(int numberOfPage)
        {
            var result = new List<string>();
            var startPage = (numberOfPage - 1) * 150;
            for (var i = startPage; i < 150 * numberOfPage; i++)
            {
                result.Add(WordsList[i]);
            }
            return result;
        }

        public void OnPostAddOrRemoveWord()
        {
            var specificWord = GetWordWithoutSimbols(Word, out (int, char)? simbols);
            if (!_userService.IsUserStudingThisWord(User.Login, specificWord) && !_userService.IsUserKnownThisWord(User.Login, specificWord))
            {
                _studiedWordService.SaveNewStudiedWordToDb(specificWord, User);
                MessageRelatedWithWord = $"������ ����� ({specificWord}) ��������� � ���������";
            }
            else if (_userService.IsUserStudingThisWord(User.Login, specificWord) && !_userService.IsUserKnownThisWord(User.Login, specificWord))
            {
                _knownWordService.SaveNewKnownWordToDb(specificWord, User);
                _studiedWordService.RemoveUserFromStudiedWordInDb(specificWord, User);
            }
            else
            {
                _knownWordService.RemoveUserFromKnownWordInDb(specificWord, User);
            }
            OpenPage(numberOfPage);
        }

        public void OnPostNextPage()
        {
            if (!VisitedPages.Contains(numberOfPage))
            {
                VisitedPages.Add(numberOfPage);
                for (var i = 0; i < WordsOnPage.Count(); i++)
                {
                    if (WordsOnPage[i].Contains("\n"))
                    {
                        var twoWords = MakeNewLine(WordsOnPage[i]);
                        for (var y = 0; y < twoWords.Count(); y++)
                        {
                            var specificWord = GetWordWithoutSimbols(twoWords[y], out (int, char)? simbols);
                            if (!_userService.IsUserStudingThisWord(User.Login, specificWord) && !_userService.IsUserKnownThisWord(User.Login, specificWord))
                            {
                                _knownWordService.SaveNewKnownWordToDb(specificWord, User);
                            }
                        }
                    }
                    else
                    {
                        var specificWord = GetWordWithoutSimbols(WordsOnPage[i], out (int, char)? simbols);
                        if (!_userService.IsUserStudingThisWord(User.Login, specificWord) && !_userService.IsUserKnownThisWord(User.Login, specificWord))
                        {
                            _knownWordService.SaveNewKnownWordToDb(specificWord, User);
                        }
                    }
                }
            }
            numberOfPage++;
            OpenPage(numberOfPage);
        }

        public void OnPostPreviousPage()
        {
            numberOfPage--;
            OpenPage(numberOfPage);
        }

        public void OnPostMoveToPageNumber()
        {
            numberOfPage = PageNumber;
            OpenPage(numberOfPage);
        }

        public async Task OnPostTranslateWord()
        {
            var tw = await BusinessLogic.Translator.TranslateWord(Word, Language);
            TranslatedWord = $"�������: {Word} (ru) - {tw} ({Language})";
            OpenPage(numberOfPage);
        }

        public string GetWordWithoutSimbols(string someWord, out (int, char)? simbols)
        {
            var word = someWord.ToCharArray();
            var result = string.Empty;
            simbols = null;

            foreach (var letter in word)
            {
                if (Char.IsLetter(letter))
                {
                    result += letter.ToString();
                }
                else
                {
                    simbols = (Array.IndexOf(word, letter), letter);
                }
            }
            return result.ToLower();
        }

        public string[] MakeNewLine(string word)
        {
            var betweenLineWords = word.Split('\r', '\n').ToList();
            var numberOfEmptyStrings = betweenLineWords.RemoveAll(string.IsNullOrEmpty);
            return betweenLineWords.ToArray();
        }

        public bool CheckStudiedWord(string word)
        {
            word = GetWordWithoutSimbols(word, out var simbols);
            return _userService.IsUserStudingThisWord(User.Login, word);
        }

        public bool CheckKnownWord(string word)
        {
            word = GetWordWithoutSimbols(word, out var simbols);
            return _userService.IsUserKnownThisWord(User.Login, word);
        }
    }
}
