﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$('document').ready(function () {
    var DELAY = 500, clicks = 0, timer = null;
    $('span').on('click', function (e) {
        clicks++;  //count clicks

        if (clicks === 1) {

            timer = setTimeout(function () {

                $('#word').val(e.target.textContent);
                $("#sendWord").click();
                clicks = 0;             //after action performed, reset counter

            }, DELAY);

        } else {
            clearTimeout(timer);    //prevent single-click action
            $('#wordTranslate').val(e.target.textContent);
            $("#sendWordTranslate").click(); //perform double-click action
            clicks = 0;             //after action performed, reset counter
        }

        //$('#word').val(e.target.textContent);
        //$("#sendWord").click();
    });

    $("#boxForPageNumber").change(function () {
        $('#boxForPageNumber').val(e.target.textContent);
        $("#sendPageNumber").click();
    });

    //$('span').on('dblclick', function (e) {
    //    $('#wordTranslate').val(e.target.textContent);
    //    $("#sendWordTranslate").click();
    //});

    //$.ajax({
    //    type: "POST",
    //    url: "/MainPage?handler=AddUnkownWord",
    //    beforeSend: function (xhr) {
    //        xhr.setRequestHeader("XSRF-TOKEN",
    //            $('input:hidden[name="__RequestVerificationToken"]').val());
    //    },
    //    data: JSON.stringify({ ss: e.target.innerText }),
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json"
    //});
})